<!doctype html>

<?php

/**
* Script que busca as tabelas de um banco de dados que contenha campos com o nome ou parte do nome informado
*/


// Lista as tabelas que possui ao menos um registro com nome de coluna contendo o texto de 'Column Name'

if (isset($_POST['host']) && trim($_POST['host']) != false &&
    isset($_POST['user']) && trim($_POST['user']) != false &&
    isset($_POST['password']) && trim($_POST['password']) !== false &&
    isset($_POST['database']) && trim($_POST['database']) != false &&
	isset($_POST['column_name']) && trim($_POST['column_name']) != false) {

    $host = trim($_POST['host']);
    $user = trim($_POST['user']);
    $password = trim($_POST['password']);
    $database = trim($_POST['database']);
	$column_name = trim($_POST['column_name']);;

    // Busca nos meta-dados do SGBD as tabelas que contenham campos de nome entities_id
    $con =  new mysqli($host, $user, $password, 'information_schema');
    
    // Cria os SQL para as queries nas tabelas que contenha campos entities_id
    $sql = "SELECT concat('SELECT COUNT(*) FROM ', `table_name`, ' WHERE 1=1 ', ';') AS `SQL`, `table_name`
            FROM `columns` 
            WHERE `table_schema` = '" . $database . "' 
                AND `column_name` LIKE '%" . $column_name . "%'  
            ORDER BY `table_name`;";

    $recordset = $con->query($sql);
    $sqltables = array();
	
	while ($row = $recordset->fetch_row()) {
		$sqltables[$row[1]] = $row[0];
	}

}

?>

<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>Chamados Serra Utility</title>
  <meta name="description" content="Utiliztário para a migração do Sistema de Chamados GLPI Ifes campus Serra">
  <meta name="author" content="CTI Serra">
  <link rel="stylesheet" href="css/styles.css?v=1.0">
</head>
<body>
  <script src="js/scripts.js"></script>

  <form name="frmlistar" action="" method="POST">
  <label for="host">Host: </label>
  <input type="input" name="host" id="host" value="" placeholder="ex: 172.16.98.224" title="IP ou nome do sevidor de banco de dados"><br>
  <label for="user">User: </label>
  <input type="input" name="user" id="user" value=""  placeholder="ex: root" title="Login do usuário de conexão ao banco"><br>
  <label for="password">Password: </label>
  <input type="password" name="password" id="password" placeholder="informe a senha" title="Senha do usuário de conexão"><br>
  <label for="database">Database</label>
  <input type="input" name="database" id="database" value="" placeholder="ex: chamados_db" title="Nome do banco de dados alvo da busca"><br>  
  <label for="column-name">Column Name</label>
  <input type="input" name="column_name" id="column_name" value="" title="Nome ou parte do nome da coluna que está sendo procurada" placeholder="ex: calendars_id"><br>  
  
  <input type="submit" value="Submit">
</form>
<br /><br />
<h2> Lista as tabelas que contenham alguma coluna cujo o nome contenha o texto informado em 'Column Name' </h2>


  <?php

    if (!empty($sqltables)) {

        // Banco alvo da busca
		 $con =  new mysqli($host, $user, $password, $database);

        $selectedtables = array(); 
        foreach ($sqltables as $key => $value) {
            $rstables = $con->query($value);
            $rc = 0;
            $rc = $rstables->fetch_row();
            if ($rc[0] > 0) 
                $selectedtables[$key] = '<strong>' . $key . ' </strong> contém ' . $rc[0] . ' registro(s)';
        }
        foreach ($selectedtables as $s)
            echo $s . '<br />';
    }

  ?>
</body>
</html>
